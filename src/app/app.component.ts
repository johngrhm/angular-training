import { Component, OnInit } from '@angular/core';
import * as firebase from 'firebase';
@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {
  title = 'app';
  loadedFeature = 'recipe';

  onNavigate(feature: string) {
    this.loadedFeature = feature;
  }

  ngOnInit() {
    firebase.initializeApp({
      apiKey: "AIzaSyBEEtKcAWrG90M0It-gqylK8qYMOlSdigg",
      authDomain: "ng-reciepe-book-231a5.firebaseapp.com"
    });
  }
  // My non-efficient way
  // displayPage = '';
  // onRecipeNavigated() {
  //   this.displayPage = 'recipe';
  // }

  // onShoppingNavigated() {
  //   this.displayPage = 'shopping';
  // }
}
