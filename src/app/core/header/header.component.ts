import { Response } from '@angular/http';
import { DataStorageService } from '../../shared/data-storage.service';
import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import { AuthService } from '../../auth/auth.service';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  encapsulation: ViewEncapsulation.None
})
export class HeaderComponent implements OnInit {

  constructor(private dataStorageService: DataStorageService, public authService: AuthService) { }

  ngOnInit() {
  }

  onSaveData() {
    this.dataStorageService.storeRecipes()
      .subscribe(
        (response: Response) => {
          console.log(response);
        }
      );
 }

 onLogout() {
   this.authService.logout();
 }

 onGetData() {
   this.dataStorageService.getRecipes();
 }
  // My not effecient way
  // @Output() reciepeNavigate = new EventEmitter();
  // @Output() shoppingNavigate = new EventEmitter();

  // onSelect(feature: string) {
  //   this.reciepeNavigate.emit();
  // }

  // onShoppingNavigation() {
  //   this.shoppingNavigate.emit();
  // }

}
