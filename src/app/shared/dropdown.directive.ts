import { Directive, HostBinding, OnInit, HostListener, Renderer2, ElementRef } from '@angular/core';

@Directive({
  selector: '[appDropdown]'
})
export class DropdownDirective implements OnInit {
  @HostBinding('class.open') open: boolean;
  constructor(private element: ElementRef, private renderer: Renderer2) { }

  ngOnInit() {
    this.open = false;
  }

  @HostListener('click') toggleOpen() {
    this.open = !this.open;
    // if (this.open) {
    //   this.renderer.addClass(this.element.nativeElement, 'open');
    // } else {
    //   this.renderer.removeClass(this.element.nativeElement, 'open');
    // }
  }
}
