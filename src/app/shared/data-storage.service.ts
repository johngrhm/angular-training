import { Injectable } from '@angular/core';
import { RecipeService } from '../recipe/recipe.service';
import { Recipe } from '../recipe/recipe.model';
import { AuthService } from '../auth/auth.service';
import 'rxjs/add/operator/map';
import { HttpClient, HttpParams, HttpRequest } from '@angular/common/http';
import { HttpHeaders } from '@angular/common/http/src/headers';

@Injectable()
export class DataStorageService {
    constructor(private http: HttpClient, private recipeServices: RecipeService, private authService: AuthService) {}

    storeRecipes() {
      return this.http.put('https://ng-reciepe-book-231a5.firebaseio.com/recipes.json', this.recipeServices.getRecipes(),
    {
      observe: 'body'
      // headers: new HttpHeaders().set('Authorization', 'Bearer saodifjsdiod')
    });
    // const req = new HttpRequest('PUT', 'https://ng-reciepe-book-231a5.firebaseio.com/recipes.json',
    // this.recipeServices.getRecipes(), {reportProgress: true});
    // return this.http.request(req);
    }

    getRecipes() {
      this.http.get<Recipe[]>('https://ng-reciepe-book-231a5.firebaseio.com/recipes.json')
      // this.http.get('https://ng-reciepe-book-231a5.firebaseio.com/recipes.json?auth=' + token, {
      //   observe: 'response',
      //   responseType: 'text'
      // })
      .map(
        (recipes) => {
          for (const recipe of recipes){
            if (!recipe['ingredients']) {
              console.log(recipe);
              recipe['ingredients'] = [];
               }
          }
          return recipes;
        }
      )
        .subscribe(
          (recipes: Recipe[]) => {
            this.recipeServices.setRecipes(recipes);
          }
        );
    }
}
